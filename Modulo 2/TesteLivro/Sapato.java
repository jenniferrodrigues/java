public class Sapato{
	public Long codigo;
	public String descricao;

	public String codigoCor;
	public Long tamanho;
	public double preco;

	public String genero;
	public Long quantidadeEmEstoque;

	public Sapato() {
		
	}

	public Sapato(Long novoCodigo,String novaDescricao, String novoCodigoCor,Long novoTamanho,double novoPreco,String novoGenero,Long novaQuantidadeEmEstoque) {
		codigo=novoCodigo;
		descricao=novaDescricao;
		codigoCor=novoCodigoCor;
		tamanho=novoTamanho;
		preco=novoPreco;
		genero=novoGenero;
		quantidadeEmEstoque=novaQuantidadeEmEstoque;
	}

	public String mostrarInformacoes(Long codigo,String descricao, String codigoCor,Long tamanho,Double preco,String genero,Long quantidadeEmEstoque){
		System.out.println("Codigo: " + codigo);
		System.out.println("Descricao:" + descricao);
		System.out.println("Codigo da Cor:" + codigoCor);
		System.out.println("Tamanho: " + tamanho);
		System.out.println("Preço :" + preco);
		System.out.println("Genero: " + genero);
		System.out.println("Quantidade em estoque: " + quantidadeEmEstoque);
		return descricao;

	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCodigoCor() {
		return codigoCor;
	}

	public void setCodigoCor(String codigoCor) {
		this.codigoCor = codigoCor;
	}

	public Long getTamanho() {
		return tamanho;
	}

	public void setTamanho(Long tamanho) {
		this.tamanho = tamanho;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Long getQuantidadeEmEstoque() {
		return quantidadeEmEstoque;
	}

	public void setQuantidadeEmEstoque(long quantidadeEmEstoque) {
		this.quantidadeEmEstoque = quantidadeEmEstoque;
	}

	
}