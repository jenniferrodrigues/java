public class PessoaJuridica extends Pessoa{
    private String razaoSocial;
    private String nomeFantasia;
    private String cnpj;

   
    public PessoaJuridica(){
        
    }

    public  PessoaJuridica(Long _id, String _razaoSocial,String _nomeFantasia, String _cnpj){
        super(_id);
        razaoSocial=_razaoSocial;
        nomeFantasia=_nomeFantasia;
        cnpj=_cnpj;

    }

    public void imprimirInformacoes(){
        System.out.println("---Informações da pessoa jurídica---\n");
        System.out.println("Id:"+getId());
        System.out.println("Razão social:"+getRazaoSocial());
        System.out.println("Nome fantasia:"+getNomeFantasia());
        System.out.println("CNPJ:"+cnpj);
    }

    public String getRazaoSocial() {
        return this.razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return this.nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getCnpj() {
        return this.cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    
    

}
