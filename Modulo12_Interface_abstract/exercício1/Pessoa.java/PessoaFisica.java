public class PessoaFisica extends Pessoa{
    private String nome;
    private String sobrenome;
    private String cpf;

    public PessoaFisica(){

    }

    public PessoaFisica(Long _id, String _nome,String _sobrenome, String _cpf){
        super(_id);
        nome=_nome;
        sobrenome=_sobrenome;
        cpf=_cpf;
    }

    public void imprimirInformacoes(){
        System.out.println("---Informações da pessoa---\n");
        System.out.println("Id:"+getId());
        System.out.println("Nome:"+getNome());
        System.out.println("Sobrenome:"+getSobrenome());
        System.out.println("CPF:"+getCpf());
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return this.sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getCpf() {
        return this.cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }


    
}
