import java.util.Scanner;
public class TesteClasseAbstrata {
    public static void  main(String[] args) {
        Scanner readData = new Scanner(System.in);
        //Dados pessoa fisica
       
        System.out.println("Digite o ID da pessoa física:");
        Long id= readData.nextLong();
        System.out.println("Insira o nome da pessoa física:");
        String nome = readData.next();
        System.out.println("Digite o sobrenome da pessoa física:");
        String sobrenome= readData.next();
        System.out.println("Digite o CPF da pessoa física:");
        String cpf = readData.next();

        PessoaFisica dadosPessoaFisica = new PessoaFisica(id,nome,sobrenome,cpf);


        dadosPessoaFisica.imprimirInformacoes();

        //Dados empregado
        System.out.println("Digite o ID do Empregado:");
        Long idEmpregado= readData.nextLong();
        System.out.println("Insira o nome do Empregado:");
        String nomeEmpregado = readData.next();
        System.out.println("Digite o sobrenome do Empregado:");
        String sobrenomeEmpregado= readData.next();
        System.out.println("Digite o CPF do Empregado:");
        String cpfEmpregado = readData.next();
        System.out.println("Digite o salário do Empregado:");
        Double salarioEmpregado = readData.nextDouble();
        System.out.println("Digite o código do setor salário do Empregado:");
        int codigoSetorEmpregado= readData.nextInt();
        Empregado dadosEmpregado = new Empregado(idEmpregado,nomeEmpregado,sobrenomeEmpregado,cpfEmpregado,salarioEmpregado,codigoSetorEmpregado);
        // Empregado dadosEmpregado2= new Empregado(salarioEmpregado,codigoSetorEmpregado);

        dadosEmpregado.imprimirInformacoes();
       // dadosEmpregado2.imprimirInformacoes();

  
        //Dados pessoa juridica
        System.out.println("Digite o ID da pessoa jurídica:");
        Long idPessoaJuridica= readData.nextLong();
        System.out.println("Insira a razao social da pessoa jurídica:");
        String razaoSocialPessoaJuridica = readData.next();
        System.out.println("Digite o nome fantasia da pessoa jurídica:");
        String nomeFantasiaPessoaJuridica= readData.next();
        System.out.println("Digite o CNPJ da pessoa jurídica:");
        String cnpjPessoaJuridica= readData.next();
        System.out.println("Digite o salário da pessoa jurídica:");
        
        PessoaJuridica dadosPessoaJuridica = new PessoaJuridica(idPessoaJuridica,razaoSocialPessoaJuridica,nomeFantasiaPessoaJuridica,cnpjPessoaJuridica);
        
        dadosPessoaJuridica.imprimirInformacoes();

        //Dados representante comercial

        System.out.println("Digite o ID do representante comercial:");
        Long idRepresentanteComercial= readData.nextLong();
        System.out.println("Insira a razao social do representante comercial:");
        String razaoSocialRepresentanteComercial = readData.next();
        System.out.println("Digite o nome fantasia do representante comercial:");
        String nomeFantasiaRepresentanteComercial= readData.next();
        System.out.println("Digite o CNPJ da do representante comercial::");
        String cnpjRepresentanteComercial= readData.next();
        
        System.out.println("Insira o valor do percentual de comissão do representante comercial:");
        Double percentualDeComissaoRepresentanteComercial= readData.nextDouble();
        
        RepresentanteComercial dadosRepresentanteComercial = new RepresentanteComercial(idRepresentanteComercial,razaoSocialRepresentanteComercial,nomeFantasiaRepresentanteComercial,cnpjRepresentanteComercial,percentualDeComissaoRepresentanteComercial);
        dadosRepresentanteComercial.imprimirInformacoes();
        
        // RepresentanteComercial dadoRepresentanteComercial2= new RepresentanteComercial(percentualDeComissaoRepresentanteComercial);
        // dadoRepresentanteComercial2.imprimirInformacoes();

        
    }
    
}
