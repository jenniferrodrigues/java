public class RepresentanteComercial extends PessoaJuridica {
    private Double percentualDeComissao;

    public Double getPercentualDeComissao() {
        return this.percentualDeComissao;
    }

    public void setPercentualDeComissao(Double percentualDeComissao) {
        this.percentualDeComissao = percentualDeComissao;
    }

    public RepresentanteComercial(){

    }

    public RepresentanteComercial(Long _id, String _razaoSocial,String _nomeFantasia, String _cnpj,Double _percentualDeComissao){
        super(_id,_razaoSocial,_nomeFantasia,_cnpj);
        percentualDeComissao =_percentualDeComissao;
    }

    // public RepresentanteComercial(Double _percentualDeComissao){
    //     percentualDeComissao=_percentualDeComissao;
    // }

    public void imprimirInformacoes(){
        System.out.println("---Informações do Representante comercial---\n");
        System.out.println("Id:"+getId());
        System.out.println("Razão social:"+getRazaoSocial());
        System.out.println("Nome fantasia:"+getNomeFantasia());
        System.out.println("Cnpj:"+getCnpj());
        System.out.println("Percentual de comissão:"+getPercentualDeComissao());
       
    }
}
