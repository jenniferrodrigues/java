public class Empregado extends PessoaFisica{
    private Double salario;
    private int codigoSetor;

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public int getCodigoSetor() {
        return this.codigoSetor;
    }

    public void setCodigoSetor(int codigoSetor) {
        this.codigoSetor = codigoSetor;
    }

    public Empregado(){

    }

    public Empregado(Long _id, String _nome,String _sobrenome, String _cpf,Double _salario, int _codigoSetor){
        super(_id,_nome,_sobrenome,_cpf);
        salario = _salario;
		codigoSetor = _codigoSetor;
       


    }
    // public  Empregado(Double _salario, int _codigoSetor){
    //     salario=_salario;
    //     codigoSetor=_codigoSetor;
        

    // }

    public void imprimirInformacoes(){
        System.out.println("---Informações do empregado---\n");
        System.out.println("Id:"+getId());
        System.out.println("Nome:"+getNome());
        System.out.println("Sobrenome:"+getSobrenome());
        System.out.println("CPF:"+getCpf());
        System.out.println("Salário:"+getSalario());
        System.out.println("Código do setor:"+getCodigoSetor());
    }
    
}
