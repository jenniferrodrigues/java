public abstract class Pessoa{
    protected Long id;

    public Pessoa(){

    }

    public  Pessoa(Long _id){
        id=_id;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

         
    /*Metodo abstrato nao tem implementacao*/
    public abstract void imprimirInformacoes();
}