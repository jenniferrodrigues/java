import java.util.Scanner;
public class TesteClasseAbstrata {
    public static void  main(String[] args) {
        Scanner readData = new Scanner(System.in);
        //Dados pessoa fisica
       
       
        PessoaFisica dadosPessoaFisica = new PessoaFisica(1,"Jennifer","Rodrigues","01005854009");


        dadosPessoaFisica.imprimirInformacoes();

        //Dados empregado
       
        Empregado dadosEmpregado = new Empregado(2,"joao","ferraz","054564456",400,00,2);
        // Empregado dadosEmpregado2= new Empregado(salarioEmpregado,codigoSetorEmpregado);

        dadosEmpregado.imprimirInformacoes();
       // dadosEmpregado2.imprimirInformacoes();

  
        //Dados pessoa juridica
       
        PessoaJuridica dadosPessoaJuridica = new PessoaJuridica(idPessoaJuridica,razaoSocialPessoaJuridica,nomeFantasiaPessoaJuridica,cnpjPessoaJuridica);
        
        dadosPessoaJuridica.imprimirInformacoes();

        //Dados representante comercial

        System.out.println("Digite o ID do representante comercial:");
        Long idRepresentanteComercial= readData.nextLong();
        System.out.println("Insira a razao social do representante comercial:");
        String razaoSocialRepresentanteComercial = readData.next();
        System.out.println("Digite o nome fantasia do representante comercial:");
        String nomeFantasiaRepresentanteComercial= readData.next();
        System.out.println("Digite o CNPJ da do representante comercial::");
        String cnpjRepresentanteComercial= readData.next();
        
        System.out.println("Insira o valor do percentual de comissão do representante comercial:");
        Double percentualDeComissaoRepresentanteComercial= readData.nextDouble();
        
        RepresentanteComercial dadosRepresentanteComercial = new RepresentanteComercial(idRepresentanteComercial,razaoSocialRepresentanteComercial,nomeFantasiaRepresentanteComercial,cnpjRepresentanteComercial,percentualDeComissaoRepresentanteComercial);
        dadosRepresentanteComercial.imprimirInformacoes();
        
        // RepresentanteComercial dadoRepresentanteComercial2= new RepresentanteComercial(percentualDeComissaoRepresentanteComercial);
        // dadoRepresentanteComercial2.imprimirInformacoes();

        
    }
    
}
