public class TipoDocumento {
    private long id;
    private String numero;
    

    public TipoDocumento(){
        
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    
}
