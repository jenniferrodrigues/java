public class TipoTelefone{
	private Long id;
	private String nome;


	public Long getId(){
		return id;
	}
	public void setId(Long _id){
		id = _id;
	}
	public String getNome(){
		return nome;
	}
	public void setNome(String _nome){
		nome = _nome;
	}
}