public class Telefone{
	private Long id;
	private String numero;
	private TipoTelefone tipo;
	
	public Long getId(){
		return id;
	}
	public void setId(Long _id){
		id = _id;
	}

	public String getNumero(){
		return numero;
	}

	public void setNumero(String _numero){
		numero = _numero;
	}
}