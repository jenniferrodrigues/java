public class PessoaJuridica extends Pessoa{

	private String razaoSocial;
	private String nomeFantasia;
	private String cnpj;

	public PessoaJuridica(Long _id, String _razaoSocial, String _nomeFantasia, String _cnpj){
		super(_id);
		razaoSocial = _razaoSocial;
		nomeFantasia = _nomeFantasia;
		cnpj = _cnpj;
	}

	public void setRazaoSocial(String _razaoSocial){
		razaoSocial = _razaoSocial;
	}

	public String getRazaoSocial(){
		return razaoSocial;
	}

	public void setNomeFantasia(String _nomeFantasia){
		nomeFantasia = _nomeFantasia;
	}

	public String getNomeFantasia(){
		return nomeFantasia;
	}

	public void setCnpj(String _cnpj){
		cnpj = _cnpj;
	}

	public String getCnpj(){
		return cnpj;
	}

	public void imprimirInformacoes(){
		System.out.println("Id: " + getId());
		System.out.println("Razão Social: " + getRazaoSocial());
		System.out.println("Nome Fantasia: " + getNomeFantasia());
		System.out.println("Cnpj: " + getCnpj());
	}
}