public class PessoaFisica extends Pessoa{
	private String nome;
	private String sobrenome;
	private String cpf;

	public PessoaFisica(Long _id, String _nome, String _sobrenome, String _cpf){
		super(_id);
		nome = _nome;
		sobrenome = _sobrenome;
		cpf = _cpf;
	}

	public void setNome(String _nome){
		nome = _nome;
	}

	public String getNome(){
		return nome;
	}

	public void setSobrenome(String _sobrenome){
		sobrenome = _sobrenome;
	}

	public String getSobrenome(){
		return sobrenome;
	}

	public void setCpf(String _cpf){
		cpf = _cpf;
	}

	public String getCpf(){
		return cpf;
	}
	public void imprimirInformacoes(){
		System.out.println("Id: " + getId());
		System.out.println("Nome: " + getNome());
		System.out.println("Sobrenome: " + getSobrenome());
		System.out.println("Cpf: " + getCpf());
	}
}