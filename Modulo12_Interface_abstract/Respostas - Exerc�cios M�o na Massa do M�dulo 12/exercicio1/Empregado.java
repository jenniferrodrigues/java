public class Empregado extends PessoaFisica{
	private double salario;
	private int codigoSetor;

	public Empregado(Long _id, String _nome, String _sobrenome, String _cpf, double _salario, int _codigoSetor){
		super(_id, _nome, _sobrenome, _cpf);
		salario = _salario;
		codigoSetor = _codigoSetor;
	}

	public double getSalario(){
		return salario;
	}
	public void setSalario(double _salario){
		salario = _salario;
	}

	public void setCodigoSetor(int _codigoSetor){
		codigoSetor = _codigoSetor;
	}
	public int getCodigoSetor(){
		return codigoSetor;
	}
	public void imprimirInformacoes(){
		System.out.println("Id: " + getId());
		System.out.println("Nome: " + getNome());
		System.out.println("Sobrenome: " + getSobrenome());
		System.out.println("Cpf: " + getCpf());
		System.out.println("Salario: " + getSalario());
		System.out.println("codigo Setor: " + getCodigoSetor());
	}
}