public abstract class Pessoa{
	private Long id;

	public Pessoa(Long _id){
		id = _id;
	}

	public abstract void imprimirInformacoes();

	public void setId(Long _id){
		id = _id;
	}

	public Long getId(){
		return id;
	}
}