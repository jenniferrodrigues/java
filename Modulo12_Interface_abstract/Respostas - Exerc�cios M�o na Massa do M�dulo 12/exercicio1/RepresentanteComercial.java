public class RepresentanteComercial extends PessoaJuridica{
	private double percentualDeComissao;


	public RepresentanteComercial(Long _id, String _razaoSocial, String _nomeFantasia, String _cnpj, double _percentualDeComissao){
		super(_id, _razaoSocial, _nomeFantasia, _cnpj);
		percentualDeComissao =_percentualDeComissao;
	}

	public double getPercentualDeComissao(){
		return percentualDeComissao;
	}

	public void setPercentualDeComissao(double _percentualDeComissao){
		percentualDeComissao = _percentualDeComissao;
	}

	public void imprimirInformacoes(){
		System.out.println("Id: " + getId());
		System.out.println("Razão Social: " + getRazaoSocial());
		System.out.println("Nome Fantasia: " + getNomeFantasia());
		System.out.println("Cnpj: " + getCnpj());
		System.out.println("Percentual de Comissao: " + getCnpj());
	}
}