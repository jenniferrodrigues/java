package DateFormat.CalendarFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Calendar;

public class CalendarData {

    public static void main(String[] args) {
        // Calendar c = Calendar.getInstance();
        // System.out.println("Date and atual hour: " + c.getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.set(2022, 4, 30);
        System.out.println(calendar.getTime()); // Imprime Sat Apr 30 17:01:42 BRT 2022
        calendar.add(Calendar.MONTH, 1);// add number to increments month(1)
        System.out.println(calendar.getTime()); // Imprime Sat May 30 17:01:42 BRT 2022
        System.out.println("The local data is:" + LocalDate.now());
        System.out.println("The local time is:" + LocalTime.now());
        System.out.println("The local and data is:" + LocalDateTime.now());
        System.out.println("The time zone information is:" + ZonedDateTime.now());
    }
}
