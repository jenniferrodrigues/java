package DateFormat.CalendarFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;

import javax.swing.plaf.synth.SynthSplitPaneUI;

public class CreateNewDate {

    private static final int America = 0;
    private static final int Sao_Paulo = 0;

    public static void main(String[] args) {
        // Calendar c = Calendar.getInstance();
        // System.out.println("Date and atual hour: " + c.getTime());
        LocalTime time1 = LocalTime.of(8, 15); // hora e minuto
        LocalTime time2 = LocalTime.of(8, 15, 30); // hora, minuto e segundo.
        LocalTime time3 = LocalTime.of(8, 15, 30, 200);

        // LocalDateTime dateTime1 = LocalDateTime.of(2022,Month.JANUARY, 20, 8, 15,
        // 30).
        // LocalDateTime LocalDate date1;
        // Object dateTime2 = LocalDateTime.of(date1, time1);

        // ZoneId zone = ZoneId.of(“America/Sao_Paulo”);
        // LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));
        // ZonedDateTime zoned1 = ZonedDateTime.of(2022,1,20,8,15,30,200, zone);
        // ZonedDateTime zoned2 = ZonedDateTime.of(date1, time1, zone);
        // ZonedDateTime zoned3 = ZonedDateTime.of(dateTime1, zone);

        // public static LocalTime.of(int hour, int minute);
        // public static LocalTime.of(int hour, int minute, int second);
        // public static LocalTime.of(int hour, int minute, int second, int nanos);
        // Calendar calendar = Calendar.getInstance();
        // calendar.set(2022, 4, 30);
        // System.out.println(calendar.getTime()); // Imprime Sat Apr 30 17:01:42 BRT
        // 2022
        // calendar.add(Calendar.MONTH, 1);// add number to increments month(1)
        // System.out.println(calendar.getTime()); // Imprime Sat May 30 17:01:42 BRT
        // 2022
        // System.out.println("The local data is:" + LocalDate.now());
        // System.out.println("The local time is:" + LocalTime.now());
        // System.out.println("The local and data is:" + LocalDateTime.now());
        // System.out.println("The time zone information is:" + ZonedDateTime.now());

        // ADICIONAR DATA,DIA,MES E ANO

        // LocalDate date = LocalDate.of(2022, Month.JANUARY, 20);
        // System.out.println(date); // 2022-01-20

        // date = date.plusDays(3); // adiciona dois dias
        // System.out.println(date);// 2022-01-23
        // date = date.plusWeeks(1); // adiciona uma semana
        // System.out.println(date); // 2022-01-30
        // date = date.plusMonths(1); // adiciona um mês
        // System.out.println(date); // 2022-02-28
        // date = date.plusYears(5); // adiciona 5 anos
        // System.out.println(date); // 2027-02-28

        LocalDate date = LocalDate.of(2022, Month.JANUARY, 20);
        LocalTime time = LocalTime.of(5, 15);
        LocalDateTime dateTime = LocalDateTime.of(date, time);

        System.out.println("20 de janeiro de 2022 às 5h15 será impresso no console");
        System.out.println(dateTime); // 2024-01-20T05:15

        System.out.println("subtracao de um dia");
        dateTime = dateTime.minusDays(1);
        System.out.println(dateTime); // 2024-01-19T05:15

        System.out.println("dez hora serão subtraídas da data");
        dateTime = dateTime.minusHours(10);
        System.out.println(dateTime); // 2024-01-18T19:15

        System.out.println("subtraímos 30 segundos ");
        dateTime = dateTime.minusSeconds(30);
        System.out.println(dateTime); // 2024-01-18T19:14:30

        // LocalDate date = LocalDate.of(2022, Month.JANUARY, 20);
        // LocalTime time = LocalTime.of(5,15);
        // LocalDateTime dateTime =
        // LocalDateTime.of(date,time).minusDays(1).minuaHours(10).minusSeconds(30);

    }
}
