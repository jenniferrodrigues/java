package ApiJava;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;

public class AddFormat {
    public static void main(String[] args) {
        System.out.println(Period.of(1, 2, 3));
        LocalDate date = LocalDate.of(2022, 4, 27);
        LocalTime time = LocalTime.of(6, 15);
        LocalDateTime dateTime = LocalDateTime.of(date, time);
        Period period = Period.ofMonths(1);
        System.out.println(date.plus(period)); // 2022-05-27
        System.out.println(dateTime.plus(period)); // 2022-05-27T06:15
        // System.out.println(time.plus(period)); // Exception. classe LocalTime não
        // armazena informações de data.
    }

}
