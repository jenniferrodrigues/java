package ApiJava;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

public class PerformAnimalEnrichment {

    public static void main(String[] args) {
        LocalDate start = LocalDate.of(2022, Month.JANUARY, 1);
        LocalDate end = LocalDate.of(2022, Month.MARCH, 30);
        Period period = Period.ofMonths(1); // cria uma periodo
        performAnimalEnrichment(start, end, period);
    }

    private static void performAnimalEnrichment(LocalDate start, LocalDate end,
            Period period) { // usa um período genérico
        // Formas de Period
        // Period annually = Period.ofYears(1);
        // // período de 1 ano
        // Period quarterly = Period.ofMonths(3);
        // // período de 3 meses
        // Period everyThreeWeeks = Period.ofWeeks(3);
        // // 3 semanas
        // Period everyOtherDay = Period.ofDays(2);
        // // a cada 2 dias
        // Period everyYearAndAWeek = Period.of(1, 0, 7); // Todo ano e 7 dias

        LocalDate upTo = start;
        while (upTo.isBefore(end)) {
            System.out.println("give new toy: " + upTo);
            upTo = upTo.plus(period); // adiciona um período
        }

    }
}
