package ComumExample;

import java.time.LocalDate;
import java.time.Month;

public class Monkeys {

    public static void main(String[] args) {
        var start = LocalDate.of(2022, Month.JANUARY, 1);
        var end = LocalDate.of(2022, Month.MARCH, 30);
        performAnimalEnrichment(start, end);
    }

    private static void performAnimalEnrichment(LocalDate start, LocalDate end) {
        var upTo = start;
        while (upTo.isBefore(end)) { // verifica se a data upTo é antes da data “end”
            System.out.println("Trocar o brinquedo em: " + upTo);
            upTo = upTo.plusMonths(1); // adiciona um mês
        }

    }

}