import java.util.ArrayList;
public class ClasseGenericaV2<T>{
	private T t;
	public T getT(){
			return t;
	}

	public void setT(T t){
			this.t = t;
	}

	public ArrayList<T> criarUmArrayList(T t){
		ArrayList<T> tList = new ArrayList<T>();
		tList.add(t);
		return tList ;
	}

	
/*
	public ArrayList<Cocker> criarUmArrayList(Cocker t){
		ArrayList<Cocker> tList = new ArrayList<Cocker>();
		tList.add(t);
		return tList ;	

	}
	*/
}