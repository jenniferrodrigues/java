public class ExemploMetodoGenerico2{
	public static void main(String args[]){
		Integer i = new Integer(10);
		String str = "Texto qualquer";

		//invocando o método genérico mostrarTipo
		mostrarTipo(i);
		mostrarTipo(str);
	}

	public static <Argumento> void mostrarTipo(Argumento a){
		if(a != null){
				System.out.println("Foi passado um objeto do tipo: " + a.getClass());
		}

	}
}