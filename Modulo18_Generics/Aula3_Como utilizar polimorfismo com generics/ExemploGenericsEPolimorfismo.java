import java.util.ArrayList;
public class ExemploGenericsEPolimorfismo{
	public static void main(String args[]){
		ArrayList<Cachorro> cachorros = new ArrayList<Cachorro>();
		cachorros.add(new Cocker());
		cachorros.add(new Poodle());
		cachorros.add(new Cachorro());
		listar(cachorros);
	}

	public static void listar(ArrayList<Cachorro> cachorros){
		for(Cachorro cachorro: cachorros){
			System.out.println(cachorro);
		}
	}
}