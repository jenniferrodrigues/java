/**
	Desta forma, T deve ser Number ou uma classe filha de Number.
	Assim, conseguiremos limitar o tipo de dados que a classe genérica
	FuncoesNumericas aceita.
*/
//declararcao de classe generica que aceita somente os dados da classe requerida
//qdo quer declarar uma classe generica,mas quer bloquear para que o tpo (T) seja filho de uma classe especificado
//no caso classe Number.Deve usar generics(T extends Number)
public class FuncoesNumericas<T extends Number>{
		private T num;
		FuncoesNumericas(T n){
				num = n;
		}

		public boolean ehInteiro(){
			boolean result = false;
			//se o resto da divisão de num por 1 for zero, ele é um inteiro
			// pode usar double, pois a classe filha está referenciada para number. 
			//tem o metodo
			if(num.doubleValue() % 1 == 0){ //ok, pois Number tem o método doubleValue()
				result = true;
			}
			return result;
		}

		public boolean ehDecimal(){
			boolean result = false;
			if(!ehInteiro()){
				result = true;

			}
			return result;
		}

		public static void main(String args[]){
			FuncoesNumericas<Integer>  fns = new FuncoesNumericas<Integer>(5);
			System.out.println(" Eh inteiro: " + fns.ehInteiro()); 
			System.out.println(" Eh decimal: " + fns.ehDecimal()); 

			//não compilaria, pois o compilador sabe que essa declaração soh vai aceitar Integer.
			//FuncoesNumericas<Integer>  fns2 = new FuncoesNumericas<Integer>(5.0);

			FuncoesNumericas<Double>  fns3 = new FuncoesNumericas<Double>(5.5);//pode passar double, pq e uma classe generica. Aceita o double.s
			System.out.println(" Fns3 Eh inteiro: " + fns3.ehInteiro()); 
			System.out.println(" Fns3 Eh decimal: " + fns3.ehDecimal()); 
		}
	
}