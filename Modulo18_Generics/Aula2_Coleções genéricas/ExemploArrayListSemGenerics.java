import java.util.ArrayList;

public class ExemploArrayListSemGenerics{
	public static void main(String args[]){
		ArrayList nomes = new ArrayList();
		nomes.add("Bruno");
		nomes.add("Jose");
		nomes.add("Rafael");
		nomes.add(5.9);
		nomes.add(5);

		listar(nomes);
	}

	public static void listar(ArrayList nomes){
		//Recupera um Object da lista
		for(Object nome: nomes){
			//É necessário converter Object para String (Casting obrigatório)
			System.out.println("Nome: " + (String)nome); //vai dar ruim =(
		}
	}
}