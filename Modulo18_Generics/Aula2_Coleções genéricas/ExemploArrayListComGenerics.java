import java.util.ArrayList;

public class ExemploArrayListComGenerics{
	public static void main(String args[]){
		ArrayList<String> nomes = new ArrayList<String>();
		nomes.add("Bruno");
		nomes.add("Jose");
		nomes.add("Rafael");
		// nomes.add(5.9); //não compilaria
		// nomes.add(5); //não compilaria

		listar(nomes);
	}

	public static void listar(ArrayList<String> nomes){
		//Recupera um Object da lista
		for(String nome: nomes){
			//Não é necessário converter Object para String 
			System.out.println("Nome: " + nome); //vai dar ruim =(
		}
	}
}