import java.util.ArrayList;
import java.util.List;

class ClasseA{
	public String toString(){
		return this.getClass().getName();
	}
}
class ClasseB extends ClasseA{

}
class ClasseC extends ClasseB{

}
class ClasseD extends ClasseC{

}
class ClasseE{
		public String toString(){
		return this.getClass().getName();
	}

}

public class WildcardBoundsExamples{
	public static void main(String args[]){

		ClasseA a = new ClasseA();
		ClasseB b = new ClasseB();
		ClasseC c = new ClasseC();
		ClasseD d = new ClasseD();
		ClasseE e = new ClasseE();

		// ? super ClasseB aceita qualquer objeto que seja subclasse da ClasseB ou a própria classe ClasseB
		ArrayList<? super ClasseB> listaCoringaSuper = new ArrayList<>();
		listaCoringaSuper.add(b); //ok
		listaCoringaSuper.add(c); //ok, é subclasse de ClasseB 
        //listaCoringaSuper.add(a); // não compilaria

		// ? extends ClasseB aceita qualquer objeto que seja subclasse da ClasseB ou a própria classe ClasseB
        ArrayList<? extends ClasseB> listaCoringaExtends = new ArrayList<>(); //torna a lista somente leitura
     //   listaCoringaExtends.add(a);
      //  listaCoringaExtends.add(b);
      // listaCoringaExtends.add(c); //não compila, pois ClasseC não é superclasse de ClasseB nem é a própria ClasseB

        List<ClasseB> listaDeClasseB = new ArrayList<>();
        listaDeClasseB.add(b);
        List<ClasseA> listaDeClasseA = new ArrayList<>();
        listaDeClasseA.add(a);
        List<ClasseC> listaDeClasseC = new ArrayList<>();
        listaDeClasseC.add(c);

        upperBoundPrintList(listaDeClasseB);
        
       	upperBoundPrintList(listaDeClasseC);

       	lowerBoundPrintList(listaDeClasseB);
	}
	/**
			Aceita lista de ClasseB ou de qualquer objeto que seja filho de ClasseB
	*/
	public static void upperBoundPrintList(List<? extends ClasseB> lista){
		System.out.println("Executando upperBoundPrintList");
		for (ClasseB b: lista) {
			System.out.println(b);
			
		}
	}

	/**
			Aceita lista de ClasseB ou de qualquer objeto que seja pai de ClasseB
	*/
	public static void lowerBoundPrintList(List<? super ClasseB> lista){
		System.out.println("Executando lowerBoundPrintList");
		for (Object b: lista) {
			System.out.println(b);
			
		}
	}
}