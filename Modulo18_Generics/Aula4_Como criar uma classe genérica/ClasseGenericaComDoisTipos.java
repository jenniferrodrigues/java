// libs para importar o map
import java.util.Map;
import java.util.HashMap;
public class ClasseGenericaComDoisTipos<T, V>{
	private T t;
	private V v;
	public T getT(){
			return t;
	}

	public void setT(T t){
			this.t = t;
	}
	public V getV(){
			return v;
	}
	public void setV(V v){
			this.v = v;
	}

	// se fosse implementar o mapa generico conforme o exemplo 
	public V get(Object key){
		// implementar logica
		return v;
	}

	// classe para usar a fcoa Map
	public static void main(String ...args){
		// Assim que usa generic. Alguem ja declarou na api do java essa classe
		// garante que os valores das colecctions seguem exatamente
		// o tipo definido quando definimos a collection
		Map<String, String> mapa = new HashMap<String,String>();
		// poderia ter:
		// List<Pessoa>pessoas;//nessa lista tera somente esse tipo de objeto
		// List<Cachorro> cachorro;//nessa lista tera somente esse tipo de objeto
		// Garante que tera somente o tipo de objeto dentro da lista
		mapa.put("000.000.000.00", "Naomy");
		mapa.put("000.000.000.01", "Angelo");
		mapa.put("000.000.000.02", "Jennifer");

		String Naomy =mapa.get("000.000.000.00");
		System.out.println("Nome selecionado:"+Naomy);
	}
}