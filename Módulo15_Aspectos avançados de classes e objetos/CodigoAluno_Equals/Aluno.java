public class Aluno{
    private String nome;
    private String sobrenome;

    public Aluno(String nome,String sobrenome){
        this.nome=nome;
        this.sobrenome=sobrenome;
    }

    public boolean equals(Object obj){
        boolean result= false;
        if(obj instanceof Aluno){
            //faz a conversao
            Aluno aluno =(Aluno)obj;
            //compara se o nome colocado em Aluno aluno =(Aluno)obj;
            // e o nome passado como parametro no equals  sao iguais.
            //Faz a mesma comparação com sobrenome
            if(this.nome.equals(aluno.nome)&& this.sobrenome.equals(aluno.sobrenome)){
                result =true;
            }else{
                result=false;
            }
        }else{
            result=false;
        }
        return result;
    }
}