public class TesteEquals{
    public static void main(String[] args) {
        Aluno aluno1= new Aluno("José", "Rodrigues");
        Aluno aluno2 = new Aluno("José", "Rodrigues");

        //Compara se os objetos são iguais. Retorna true
        System.out.println("Comparação com Equals:"+aluno1.equals(aluno2));
        //O objeto aluno1 é diferente do objeto  aluno2 em memoria. 
        //retorna falso
        System.out.println("Comparação com == :"+(aluno1==aluno2));
        
    }
}