public class ClasseA{
    public String nome ="Joseph";
    public static String sobrenome = "Climber";
    static{
        System.out.println("ClasseA-Executando bloco de inicialização estático 1");
        sobrenome="da silva";
    
    }
    static{
        System.out.println("ClasseA-Executando bloco de inicialização estático 2");
        sobrenome="da Silva Sauro";
    }
    public ClasseA(){
        System.out.println("Executando construtor da classe A");
    }
}