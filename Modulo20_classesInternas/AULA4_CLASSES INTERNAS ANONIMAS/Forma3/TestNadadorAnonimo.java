public class TestNadadorAnonimo{
	public static void fazerNadar(Nadador nadador){
		nadador.nadar();
	}

	public static void main(String[] args){
		fazerNadar(new Nadador(){
			//corpo da classe interna anonima
			public void nadar(){
				System.out.println("UAU! Estou nadando como NUNCA VISTO!");	
			}
		});
		
	}
}