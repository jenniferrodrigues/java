public class NadadorAnonimo{
	public static void main(String[] args){
		//nao poderia fazer uma interface new Nadador,
		
		Nadador nadador = new Nadador(){
			//mas estamos os referindo a uma classe interna anonima
			public void nadar(){
				System.out.println("Uau! Estou nadando!");
			}
		};
		nadador.nadar();
	}
}