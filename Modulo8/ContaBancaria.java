package Modulo8;

public class ContaBancaria{
	private String nomeDoCorrentista;
	private String nomeDoBanco;
	private Integer numeroDaAgencia;
	private Integer numeroDaContaCorrente;
	private Double saldoAtual;
	private String cpf;

public ContaBancaria() {
	
}
public ContaBancaria(String novoNomeDoCorrentista, String novoNomeDoBanco, Integer novoNumeroDaAgencia, Integer novoNumeroDaContaCorrente, Double novoSaldoAtual){

	nomeDoCorrentista = novoNomeDoCorrentista;
	nomeDoBanco = novoNomeDoBanco;
	numeroDaAgencia = novoNumeroDaAgencia;
	numeroDaContaCorrente = novoNumeroDaContaCorrente;
	saldoAtual = novoSaldoAtual;

}
public ContaBancaria(String novoNomeDoCorrentista, String novoNomeDoBanco, Integer novoNumeroDaAgencia, Integer novoNumeroDaContaCorrente, Double novoSaldoAtual, String _cpf){

	nomeDoCorrentista = novoNomeDoCorrentista;
	nomeDoBanco = novoNomeDoBanco;
	numeroDaAgencia = novoNumeroDaAgencia;
	numeroDaContaCorrente = novoNumeroDaContaCorrente;
	saldoAtual = novoSaldoAtual;
	cpf = _cpf;

}
public void setNomeDoCorrentista(String _nome){
	nomeDoCorrentista = _nome;
}

public void setNomeDoBanco(String _nomeDoBanco){
	nomeDoBanco = _nomeDoBanco;
}

public void setNumeroDaAgencia(Integer _numeroDaAgencia){
	numeroDaAgencia = _numeroDaAgencia;
}

public void setNumeroDaContaCorrente(Integer _numeroDaContaCorrente){
	numeroDaContaCorrente = _numeroDaContaCorrente;
}

public void setSaldoAtual(Double _saldoAtual){
	saldoAtual = _saldoAtual;
}

public String getNomeDoCorrentista(){
	return nomeDoCorrentista;
}
public String getNomeDoBanco(){
	return nomeDoBanco;
}

public Integer getNumeroDaAgencia(){
	return numeroDaAgencia;
}

public Integer getNumeroDaContaCorrente(){
	return numeroDaContaCorrente;
}


public ContaBancaria criarContaBancaria(String novoNomeDoCorrentista, String novoNomeDoBanco, Integer novoNumeroDaAgencia, Integer novoNumeroDaContaCorrente, Double novoSaldoAtual){
	ContaBancaria conta = new ContaBancaria();
	conta.setNomeDoCorrentista(novoNomeDoCorrentista);
	conta.setNomeDoBanco(novoNomeDoBanco);
	conta.setNumeroDaAgencia(novoNumeroDaAgencia);
	conta.setNumeroDaContaCorrente(novoNumeroDaContaCorrente);
	conta.setSaldoAtual(novoSaldoAtual);
	return conta;
}

public void depositar(Double valorDoDeposito){
	saldoAtual = saldoAtual + valorDoDeposito;
	System.out.println("Novo Saldo: " + saldoAtual);
}

public void efetuarSaque(Double valorDoSaque){
	if(valorDoSaque > saldoAtual){
		System.out.println("Você não tem saldo suficiente para sacar " + valorDoSaque + ". Você tem apenas: " + saldoAtual);
	}else{
		saldoAtual = saldoAtual - valorDoSaque;
		System.out.println("Saque de R$ " + valorDoSaque + " efetuado com sucesso!");
		System.out.println("Seu saldo disponível em conta: R$ " + saldoAtual);
	}
}

}