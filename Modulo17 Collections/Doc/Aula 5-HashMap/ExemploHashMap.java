import java.util.Map;
import java.util.HashMap;

public class ExemploHashMap{
	
	public static void main(String ...args){

		Map agenda = new HashMap();

		//adicionar numeros na agenda:chave,valor
		agenda.put("Jose", "(19) 99929-9991");
		agenda.put("Joao", "(19) 99919-9992");
		agenda.put("Tiago", "(19) 99909-9993");
		agenda.put("Jennifer","(48) 991751885");

		//percorre o map usando um loop for e imprime os contatos objetos do map (agenda)
		listarContatosDaAgenda(agenda);


		//sobrepoe o numero do telefone do josé
		agenda.put("Jose", "(48) 991751885");

		//buscar
		String numeroDoJose = (String)agenda.get("Jose"); //o retorno é sempre um Object, portanto você precisa tomar cuidado e fazer cast qdo necessário
		System.out.println("Numero do Jose: " + numeroDoJose);
		String numeroDaJennifer =(String)agenda.get("Jennifer");
		System.out.println("Numero da Jennifer:"+numeroDaJennifer);

		//listar
		listarContatosDaAgenda(agenda);

		//excluir item ( contato do Jose)
		System.out.println("Excluindo o contato do Jose");
		agenda.remove("Jose");
		System.out.println("Excluindo o contato do Jennifer");
		agenda.remove("Jennifer");
		
		listarContatosDaAgenda(agenda);

	}

	public static void listarContatosDaAgenda(Map agenda){
		System.out.println("### Listando os contatos da agenda ###\n\n");
		if(agenda != null && !agenda.isEmpty()){
			//imprime o tamanho da agenda
			System.out.println("Quantidade de contatos for: " + agenda.size());

			//percorre a agenda, a ordem é impossivel de determinar
			for(Object key: agenda.keySet()){
				System.out.println(key + " Telefone lido: "  + agenda.get(key));
				if(key=="Joao"){
					System.out.println(key + " Telefone do Joao percorrido é: "  + agenda.get(key));
				}
			}
		}else{
			System.out.println("Agenda esta vazia!");
		}
		System.out.println("### Fim da lista de contatos da agenda ###");
		System.out.println("##########################################\n\n");
	
	}
}