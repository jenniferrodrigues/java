import java.util.ArrayList;
import java.util.List;

public class ExemploArrayList{
	public static void main(String ...args){
		List equipe = new ArrayList();
		
		//Adicionando elementos no ArrayList
		equipe.add("Durvalino"); //adiciona no final da lista e retorna true caso a inserção seja feita
		equipe.add("Etelvino");
		equipe.add("João");
		equipe.add("Jose");
	
		equipe.add("Maria");

		listarTimeComForOtimizado(equipe);

		//Exclui a maria da equipe
		equipe.remove("Maria"); //remove o objeto da lista caso exista e retornao objeto removido
		
		

		listarTimeComFor(equipe);

		//Verifica se o elemento existe na lista
		boolean joseEstaNaLista = equipe.contains("Jose");
		System.out.println("Jose esta na lista? " + joseEstaNaLista );

		equipe.clear(); //exclui todos da lista equipe

		listarTimeComForOtimizado(equipe);

	}	

	public static void listarTimeComForOtimizado(List time){
		System.out.println(" ---listarTimeComForOtimizado--- ");
		if(time != null && !time.isEmpty()){ //verifica se a lista está vazia
			for(Object nome: time){
				System.out.println("Nome: " + (String)nome);
			}
		}else{
			System.out.println("A lista da equipe esta vazia.");
		}
	}

	public static void listarTimeComFor(List time){
		System.out.println(" ---listarTimeComFor--- ");
		if(time != null && !time.isEmpty()){ //verifica se a lista está vazia
			for(int i = 0; i< time.size(); i++){
				String nome = (String)time.get(i);
				System.out.println("Nome: " + nome);
			}
		}else{
			System.out.println("A lista da equipe esta vazia.");
		}
	}
}