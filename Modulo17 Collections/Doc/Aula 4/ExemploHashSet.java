import java.util.HashSet;
import java.util.Set;

public class ExemploHashSet{
	public static void main(String ...args){
		//Set-para garantir que nao havera elementos duplciados na lista.ashSet aceita qualque r object como elemento
		//Pode adicionar qualquer elemento:string, caneta,aviao,numero
		Set dvds = new HashSet();
		
		//Adicionando elementos no ArrayList
		dvds.add("Tiao Carreiro e Pardinho"); //adiciona no final da lista e retorna true caso a inserção seja feita
		dvds.add("Tonico e Tinoco");
		dvds.add("Chitaozinho e Xororo");
		dvds.add("Chico Rey e Parana");
		dvds.add("Exaltasamba");
		
		System.out.println("Tamanho da lista de dvds: " + dvds.size());

		listarDvdsComForOtimizado(dvds);

		//Tenta adicionar um dvd duplicado
		dvds.add("Chico Rey e Parana"); //nao adiciona pois esse dvd ja existe no hashset

		System.out.println("Tamanho da lista de dvds: " + dvds.size());

		listarDvdsComForOtimizado(dvds);

		//Verifica se ja tenho um dvd do Tonico e Tinoco
		boolean jaTenhoTonicoETinoco = dvds.contains("Tonico e Tinoco");
		System.out.println("Ja tenho dvd do Tonico e Tinoco? " + jaTenhoTonicoETinoco );

		dvds.clear(); //exclui todos da lista 

		listarDvdsComForOtimizado(dvds);

	}	

	public static void listarDvdsComForOtimizado(Set dvds){
		System.out.println(" ---listarDvdsComForOtimizado--- ");
		if(dvds != null && !dvds.isEmpty()){ //verifica se a lista está vazia
			for(Object nome: dvds){
				System.out.println("Nome: " + (String)nome);
			}
		}else{
			System.out.println("A lista de dvds esta vazia.");
		}
	}

}