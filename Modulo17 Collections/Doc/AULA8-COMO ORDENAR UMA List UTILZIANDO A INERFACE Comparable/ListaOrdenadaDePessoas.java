import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class ListaOrdenadaDePessoas{
	public static void main(String ...args){
		List pessoas = new ArrayList();

		PessoaFisica pessoa1 = new PessoaFisica(1l, "Jennifer");
		PessoaFisica pessoa2 = new PessoaFisica(2l, "Naomy");
		PessoaFisica pessoa3 = new PessoaFisica(3l, "Vera");
		PessoaFisica pessoa4 = new PessoaFisica(4l, "Daimon");

		pessoas.add(pessoa1);
		pessoas.add(pessoa2);
		pessoas.add(pessoa3);
		pessoas.add(pessoa4);
		imprimirPessoas(pessoas);

		//coloca as pessoas da lista em ordem pelo nome
		Collections.sort(pessoas);
		
		System.out.println("\n\n");
		System.out.println("Imprimindo a lista ordenada na ordem ascendente");
		imprimirPessoas(pessoas);

		//colocar as pessoas em ordem descendente
		Collections.reverse(pessoas);

		System.out.println("\n\n");
		System.out.println("Imprimindo a lista ordenada na ordem descendente/reversa");
		imprimirPessoas(pessoas);

	}

	public static void imprimirPessoas(List pessoas){
		System.out.println("imprimindo lista de pessoas");
		for(Object obj: pessoas){
			PessoaFisica pessoa = (PessoaFisica)obj;
			System.out.println(pessoa.getNome());
		}
	}
}