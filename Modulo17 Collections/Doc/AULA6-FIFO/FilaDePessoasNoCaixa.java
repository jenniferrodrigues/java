import java.util.Queue;
import java.util.LinkedList;
public class FilaDePessoasNoCaixa{
	public static void main(String... args){
			Queue filaDePessoas = new LinkedList();

			filaDePessoas.add("Jennifer");
			filaDePessoas.add("Naomy");
			filaDePessoas.add("Vera");
			filaDePessoas.add("Daimon");
			filaDePessoas.add("Maria");
			System.out.println("Numero de pessoas na fila: " + filaDePessoas.size());
			do{
					String proximoCliente = (String)filaDePessoas.poll(); //remove o proximo da file de acordo com a ordem de chegada
					System.out.println(proximoCliente + " dirija-se ao caixa 1");
					System.out.println("Numero de pessoas na fila: " + filaDePessoas.size());
			}while(!filaDePessoas.isEmpty());
	}
}