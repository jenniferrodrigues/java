import java.util.Deque;
import java.util.ArrayDeque;
import java.util.Iterator;

public class PilhaDeLivros{
	public static void main(String... args){
			Deque pilhaDeLivros = new ArrayDeque(); //por padrao a capacidade é de 16 elementos

			System.out.println("Vamos comecar a empilhar livros...\n\n");
			pilhaDeLivros.addFirst("Livro 1");
			pilhaDeLivros.addFirst("Livro 2");
			pilhaDeLivros.addFirst("Livro 3");
			pilhaDeLivros.addFirst("Livro 4");
			pilhaDeLivros.addFirst("Livro 5");
			System.out.println("Numero de livros na pilha: " + pilhaDeLivros.size());
			System.out.println("\n\nVamos percorrer a pilha de livros sem retirar nada dela");


			//vamos percorrer a pilha de livros sem mexer nela!
			Iterator iteratorPilhaDeLivros = pilhaDeLivros.iterator(); //retorna o iterator do ArrayDeque
			while(iteratorPilhaDeLivros.hasNext()) {
         		String livro = (String)iteratorPilhaDeLivros.next();
         		System.out.println(livro );
      		}



			System.out.println("Vamos comecar a retirar os livros da pilha....\n\n");
			do{
					String proximoLivro = (String)pilhaDeLivros.removeFirst(); //remove o livro de cima da pilha (LIFO)
					System.out.println(proximoLivro );
					
			}while(!pilhaDeLivros.isEmpty());
			System.out.println("\n\nNumero de livros na pilha: " + pilhaDeLivros.size());
	}
}