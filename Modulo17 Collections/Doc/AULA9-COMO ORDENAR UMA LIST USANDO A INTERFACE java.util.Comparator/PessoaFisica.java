
public class PessoaFisica implements Comparable{
	private Long id;
	private String nome;
	private String sobrenome;

	public Long getId(){
		return id;
	}

	public String getNome(){
		return nome;
	}

	public void setSobrenome(String sobrenome){
		this.sobrenome = sobrenome;
	}

	public String getSobrenome(){
		return this.sobrenome;
	}

	public void setId(Long id){
		this.id = id;
	}
	public void setNome(String nome){
		this.nome = nome;
	}

	public PessoaFisica(Long id, String nome){
		this.id = id;
		this.nome = nome;
	}
	public PessoaFisica(Long id, String nome, String sobrenome){
		this.id = id;
		this.nome = nome;
		this.sobrenome = sobrenome;
	}

	public int compareTo(Object obj){
		/**
			REGRAS

			REGRA 1: Se forem iguais deve retornar 0.
			REGRA 2: Deve retornar -1 se for menor
			REGRA 3: Deve retornar 1 se for maior
		*/
		int resultado = 0;
		if(obj != null ){
			PessoaFisica pessoa = (PessoaFisica)obj;
			if(this.id.equals(pessoa.getId())){
				resultado = 0;
			}else{
				//utiliza o compareTo ja implementado da classe String
				resultado = this.getNome().compareTo(pessoa.getNome());
			}
		}
	
		return resultado;
	}
}