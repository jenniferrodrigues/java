import java.util.Comparator;
public class PessoaFisicaOrdenadaPorSobrenomeComparator implements Comparator{
    public int compare(Object obj1, Object obj2){
        int result = 0;
        PessoaFisica pessoa1= null;
        PessoaFisica pessoa2= null;
        
        if(obj1!= null){
            pessoa1=(PessoaFisica)obj1;
        }
        if(obj2!= null){
            pessoa2=(PessoaFisica)obj2;
        }
        if(pessoa1!=null && pessoa2!=null){
            result =pessoa1.getSobrenome().compareTo(pessoa2.getSobrenome());
        }
        return result;
}  
}
