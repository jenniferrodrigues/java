public enum Tamanho{
    PEQUENO(1,200),MEDIO(2,500),GRANDE(3,900);
    //REPRESENTA A QUANTIDADE EM ml CONTIDA NESTE TAMANHO
    private int quantidadeEmMl;
    private int id;

    //se nao usasse enum a parte PEQUENO(1,200),MEDIO(2,500),GRANDE(3,900);
    //seria: 
    //private Tamanho PEQUENO=new Tamanho(1,200)
    //private Tamanho MEDIO=new Tamanho(2,500)
    //private Tamanho GRANDE=new Tamanho(3,900) 
    public int getQuantidadeEmMl(){
        return quantidadeEmMl;
    }
    public int getId(){
        return id;
    }

    /**
     * construtor que recebe id e quantidade em ML que representa 
     * Somente o construtor pode chamar o enum. Por exemplo,quando 
     * chama o tamanho PEQUENO busca os valores (1,200)
     */
    private Tamanho(int id, int quantidadeEmMl){
        this.id=id;this.quantidadeEmMl=quantidadeEmMl;
        
        
    }
}