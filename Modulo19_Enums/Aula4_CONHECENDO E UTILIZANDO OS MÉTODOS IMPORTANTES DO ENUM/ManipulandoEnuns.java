public class ManipulandoEnuns{
    
    public static void main(String[] args) {
        //obtem um array com todos os valores do enum na ordem que foram declaraados
        Tamanho[] valoresDoEnum = Tamanho.values();

        //vamos perorrer todos os valores do enum
        for (Tamanho t:valoresDoEnum){
            //imprime o nome do Enum
            System.out.println("Imprimindo dados enum:"+t.name());

            //retorna o nome da constante do enum
            System.out.println("name:"+t.name());

            //retorna o mesmo que o método name()
            System.out.println("toString:"+t.toString());

            //retorna a ordem do valor declarado no enum(inicia em 0)
            System.out.println("ordinal:"+t.ordinal());
        }

        //obtem o objeto enum que representa a string PEQUENO no enum
        Tamanho tamanho=Tamanho.valueOf("PEQUENO");//cuidado.Lança exceção Illegalxxx se o valor nao for o esperado. Por ex.: pequeno
        System.out.println("Tamanho:"+tamanho);
        
    }
   

}