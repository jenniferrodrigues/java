public class MilkShake{
    private int id;
    private Tamanho tamanho;
    private String sabor;
    //metodos getters and setters
    public void setId(int id){
        this.id=id;
    }
    public void setTamanho(Tamanho tamanho){
        this.tamanho=tamanho;
    }

    public void setSabor(String sabor){
        this.sabor=sabor;
    }

    public String getSabor(){
        return sabor;
    }
    public int getId(){
        return id;
    }

    public Tamanho getTamanho() {
        return tamanho;
    }
    /**
     * Exibe os dados do milkshake no console
     */
    public void mostrasDadosNoConsole(){
        System.out.println("Id:"+getId());
        System.out.println("Tamanho:"+getTamanho());
        System.out.println("Quantidade em Ml:"+getTamanho().getQuantidadeEmMl()+"ml");
        System.out.println("Sabor:"+getSabor()+"\n");
        

    }

    public static void main(String[] args) {
        MilkShake milkShake1=new MilkShake();
        milkShake1.setId(1);
        milkShake1.setSabor("Chocolate");
        milkShake1.setTamanho(Tamanho.PEQUENO);

        MilkShake milkShake2=new MilkShake();
        milkShake2.setId(2);
        milkShake2.setSabor("coco");
        milkShake2.setTamanho(Tamanho.GRANDE);

        milkShake1.mostrasDadosNoConsole();

        milkShake2.mostrasDadosNoConsole();
        
    }
   

}