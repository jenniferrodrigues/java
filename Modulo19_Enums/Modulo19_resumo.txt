-Aula2-O QUE SAO ENUMS?
Criado após Java 5.
USADO PARA:
-Limitar os valores para uma variável
-Ter uma certa tipagem
1)O que sao?
-maneira elegante de declarar constantes e restringir os valores possíveis de uma variavel;
-Os Enums surgiram no Java a partir da versão 5
2)Quando é indicado o uso de enums?
Quando tiver um úmeros fixo de possíveis valores para uma variável.
Ex.de uso:
-TIPO DE DOCUMENTO:CPF,CNPJ,CNH
-TIPO DE CONTATO:CELULAR,FICO,TRABALHO
-SEXO:MASCULINO,FEMININO
3)HÁ 3 FORMAS DIFERENTES DE REALIZAR OS ENUMS:
FORMA1:Como um arquivo de classe separado:FORMA USADA NO DIA A DIA
*Classe sexo.JAVA:
public enum Sexo{
//variavel constante.Por convencao declarar maiuscula
	MASCULINO,FEMININO}
	
*Classe Pessoa.java
Quando usa o Enum nao precisa tratar a variavel sexo para m maiusculo, primeira letra maiuscula restantes minusculas...
Bloqueia que outro programar coloque valores incorretos para o sexo.

public class Pessoa{
//permitidos somente MASCULINO E FEMININO
  private Sexo sexo;
public void setSexo(Sexo sexo){
this.sexo =sexo;}
public Sexo getSexo(){
	return sexo;}
public static void main(String[] args){
	Pessoa pessoa = new Pessoa();
	//ja pensou ter que tratar todos os possíveis valores?
	pessoa.setSexo(Sexo.FEMININO);

	System.out.println("Sexo da pessoa:" + pessoa.getSexo());}}

FORMA2:NO MESMO ARQUIVO DE OUTRA CLASSE

enum Sexo{
    MASCULINO,FEMININO
}
public class Pessoa{
    //permitidos somente MASCULINO E FEMININO
        private Sexo sexo;
        public void setSexo(Sexo sexo){
            this.sexo =sexo;}
        public Sexo getSexo(){
            return sexo;}
        public static void main(String[] args){
            Pessoa pessoa = new Pessoa();
            //ja pensou ter que tratar todos os possíveis valores?
            pessoa.setSexo(Sexo.FEMININO);
    
             System.out.println("Sexo da pessoa:" + pessoa.getSexo());
        }
}
    
FORMA3:NO CORPO DE OUTRA CLASSE


public class Pessoa{
    //permitidos somente MASCULINO E FEMININO. 
    //declarado no corpo da classe
    public enum Sexo{MASCULINO,FEMININO};
        
    private Sexo sexo;
    public void setSexo(Sexo sexo){
//aqui nao compilaria,dentro do metodo. public enum Sexo{MASCULINO,FEMININO};
        this.sexo =sexo;}
    public Sexo getSexo(){
        return sexo;}
    public static void main(String[] args){
        Pessoa pessoa = new Pessoa();
            //ja pensou ter que tratar todos os possíveis valores?
        pessoa.setSexo(Sexo.FEMININO);
    
        System.out.println("Sexo da pessoa:" + pessoa.getSexo());
        }
}
    
    
    
-REGRAS PARA DECLARAÇÃO DE ENUMS
-Enums podem ser declarados em um arquivo de codigo fonte
separado ou entao podem ser declarados dentro de uma classe 
como membro da mesma.
Enums nao podem ser declarados dentro de metodos de uma classe(somente dentro do corpo da classe)
-Enums sao constantes, portanto podem ser considerados como se fossem classes final, ou seja, nao podem ser extendidos
-Enums declarados como um arquivo de codigo fonte separado nao 
podem ser private nem protect.

AULA3-COMO CRIAR ENUMS QUE TENHAM CONSTRUTORES,CARIÁVEIS E MÉTODOS
IMAGINANDO O SEGUINTE CENÁRIO:
-Precisamos que nosso enum,tenha algumas informações importantes,como por exemplo:id e valor.
Vamos analisar como fazer isso usando enums "bombados"

-AULA4 CONHECENDO E UTILIZANDO OS MÉTODOS IMPORTANTES DO ENUM


