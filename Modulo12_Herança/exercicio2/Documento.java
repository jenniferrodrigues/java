public class Documento {
    private long id;
    private String numero;
    private TipoDocumento tipo;

    public Documento(){
        
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    
}
