import java.util.Scanner;
public class TestesMatematicos{
	public static void main(String[] args){
		int quantidadeDeRespostasCorretas = 0;
		for(int i=0; i< 10; i++){
			boolean numeroPar = i%2==0? true: false;
			if (numeroPar) {
				boolean acertou = TestesMatematicos.testeDeMultiplicacao();
				if (acertou) {
					quantidadeDeRespostasCorretas++;
				}
			}else{
				boolean acertou = TestesMatematicos.testeDeSoma();
				if (acertou) {
					quantidadeDeRespostasCorretas++;
				}
			}
		}

		System.out.println("Você acertou " + quantidadeDeRespostasCorretas + " de 10 testes.");
	 
	
	}

	public static boolean testeDeMultiplicacao(){
		boolean resultado = false;
		Scanner scanner = new Scanner(System.in);
 		int primeiroNumeroAleatorio = (int)(Math.random() * 10 ) + 1;
 		int segundoNumeroAleatorio =  (int)(Math.random() * 10 ) + 1;

		System.out.println("Quanto é " + primeiroNumeroAleatorio + " vezes " + segundoNumeroAleatorio + "? ");

		int resposta = scanner.nextInt();
		int resultadoDaMultiplicacao = primeiroNumeroAleatorio * segundoNumeroAleatorio;
		if(resposta == resultadoDaMultiplicacao){
			resultado = true;
			System.out.println("Meus parabens, você acertou!");
		}else{
			System.out.println("Ops, você errou! O resultado correto é: " + resultadoDaMultiplicacao);
		}
		return resultado;
	} 

	public static boolean testeDeSoma(){
		boolean resultado = false;
		Scanner scanner = new Scanner(System.in);
 		int primeiroNumeroAleatorio = (int)(Math.random() * 10 ) + 1;
 		int segundoNumeroAleatorio =  (int)(Math.random() * 10 ) + 1;

		System.out.println("Quanto é " + primeiroNumeroAleatorio + " mais " + segundoNumeroAleatorio + "? ");

		int resposta = scanner.nextInt();
		int resultadoDaSoma = primeiroNumeroAleatorio + segundoNumeroAleatorio;
		if(resposta == resultadoDaSoma){
			resultado = true;
			System.out.println("Meus parabens, você acertou!");
		}else{
			System.out.println("Ops, você errou! O resultado correto é: " + resultadoDaSoma);
		}
		return resultado;
	} 

}