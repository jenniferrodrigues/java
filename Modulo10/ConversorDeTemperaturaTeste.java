import java.util.Scanner;
public class ConversorDeTemperaturaTeste {
    
    
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        System.out.println("Vou solicitar alguns dados.ok?");

        System.out.println("Digite uma temperatura em graus Celsius:");
        double temperaturaCelsiusInserida=scanner.nextDouble();

        double convertidoParaFarenheit = ConversorDeTemperatura.converterCelsiusParaFahrenheit(temperaturaCelsiusInserida);
		System.out.println("Temperatura em celsius: " + temperaturaCelsiusInserida + " ==> Equivalente em fahrenheit: " + convertidoParaFarenheit);

        System.out.println("Digite uma temperatura em graus Farenheit:");
        double temperaturaFahrenheitInserida=scanner.nextDouble();

        double convertidoParaCelsius = ConversorDeTemperatura.converterFahrenheitParaCelsius(temperaturaFahrenheitInserida);
		System.out.println("Temperatura em celsius: " + temperaturaFahrenheitInserida + " ==> Equivalente em Celsius: " + convertidoParaCelsius);

   
        
    }
    
}
