public class ProvaDeMatematica{
	public static void main(String[] args){
		int quantidadeDeRespostasCorretas = 0;
		for(int i=0; i< 10; i++){
			boolean numeroPar = i%2==0? true: false;
			if (numeroPar) {
				boolean acertou = TestesMatematicos.testeDeMultiplicacao();
				if (acertou) {
					quantidadeDeRespostasCorretas++;
				}
			}else{
				boolean acertou = TestesMatematicos.testeDeSoma();
				if (acertou) {
					quantidadeDeRespostasCorretas++;
				}
			}
		}

		System.out.println("Você acertou " + quantidadeDeRespostasCorretas + " de 10 perguntas.");
	
	
	}
}