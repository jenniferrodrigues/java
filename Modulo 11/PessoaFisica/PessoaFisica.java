package PessoaFisica;

public class PessoaFisica {
    private Long id;
    private String nome;
    private String sobrenome;
    private String telefone;
    public static Long contador = (long) 0;

    public PessoaFisica() {

    }

    public static Long getContador() {
        return contador;
    }

    public static void setContador(Long contador) {
        PessoaFisica.contador = contador;
    }

    public PessoaFisica(Long novoId, String novoNome, String novoSobrenome, String novoTelefone) {
        id = novoId;
        nome = novoNome;
        sobrenome = novoSobrenome;
        telefone = novoTelefone;
    }

    public PessoaFisica dados(String novoNome, String novoSobrenome, String novoTelefone) {

        PessoaFisica pessoa = new PessoaFisica();
        // pessoa.setId(novoId);
        pessoa.setNome(novoNome);
        pessoa.setSobrenome(novoSobrenome);
        pessoa.setTelefone(novoTelefone);
        // contador ++;
        return pessoa;

    }

    public static Long contador() {
        contador++;
        return contador;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long _id) {
        id = _id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String _nome) {
        nome = _nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String _sobrenome) {
        sobrenome = _sobrenome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String _telefone) {
        telefone = _telefone;
    }

}