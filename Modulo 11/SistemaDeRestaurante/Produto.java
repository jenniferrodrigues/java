public class Produto{
	private int id;
	private String nome;
	private Double preco;

	public void setNome(String novoNome){
		nome = novoNome;
	}
	public void setPreco(Double novoPreco){
		preco = novoPreco;
	}

	public Double getPreco(){
		return preco;
	}

	public String getNome(){
		return nome; 
	}
	public int getId(){
		return id;
	}

	public void setId(int novoId){
		id = novoId; 
	}
}