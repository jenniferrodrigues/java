import java.util.Scanner;



import java.io.PrintStream;
import java.util.Random;

public class SistemaDeRestaurante {
    private static Cardapio cardapio = new Cardapio(10);
    public static  void main(String[] args) {
        Scanner readDataIn = new Scanner(System.in);
        Produto[] arrayProduto = new Produto[10];
        int i=0;
        while(true){
            exibirOpcoesDoSistema();
        }
    }

    private static void exibirOpcoesDoSistema() {
        Scanner readDataIn= new Scanner(System.in);
        

      
        System.out.println("#################");
		System.out.println(" Selecione uma opcoes abaixo: ");
		System.out.println("1 - para cadastrar novo produto");
		System.out.println("2 - para exibir cardápio");
		System.out.println("3 - para buscar preço de produto");
		System.out.println("4 - para sair do sistema");
        int opcao = readDataIn.nextInt();
        if(opcao==1){
            cadastrarNovoProduto();
        }else if(opcao==2){
            exibirCardapio();
        }else if(opcao==3){
            buscarPrecoDeProduto();
        }else if(opcao==4){
            System.out.println(" Saindo do sistema");
            System.exit(0);
        }else{
            System.out.println("OPCAO INVALIDA! Por favor selecione uma opção valida entre 1 e 4.");
			System.out.println(" Selecione uma opcoes abaixo: ");
			System.out.println("1 - para cadastrar novo produto");
			System.out.println("2 - para exibir cardápio");
			System.out.println("3 - para buscar preço de produto");
			System.out.println("4 - para sair do sistema");
        }
    


    }

    private static void cadastrarNovoProduto() {
        Scanner readDataProduct = new Scanner(System.in);
        System.out.println("Digite o nome do produto:");
        String nome = readDataProduct.next();
        System.out.println("Digite o preço do produto:");
        Double preco = readDataProduct.nextDouble();
        Produto produto = new Produto();
        produto.setNome(nome);
        produto.setPreco(preco);

    }

    private static void buscarPrecoDeProduto() {
        System.out.println("Digite o código do produto:");
        Scanner scanner = new Scanner(System.in);
        int codigoDoProduto = scanner.nextInt();
        Produto produto = cardapio.getProduto(codigoDoProduto);
        if (produto != null) {
            System.out.println("PRODUTO ENCONTRADO");
            System.out.println("Produto: " + produto.getNome() + " preco: " + produto.getPreco());
            exibirOpcoesDoSistema();
        } else {
            System.out.println("Nenhum produto encontrado!");
            exibirOpcoesDoSistema();
        }
    }

    private static void exibirCardapio() {
        Cardapio.listarProdutos();
        exibirOpcoesDoSistema();

    }
}
