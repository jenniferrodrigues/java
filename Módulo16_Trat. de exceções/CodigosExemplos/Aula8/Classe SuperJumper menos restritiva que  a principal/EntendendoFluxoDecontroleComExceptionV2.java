/*OBS: METODOS QUE IMPLEMENTAM UMA INTERFACE OU QUE SOBRESCREVEM UM METODO DE UMA CLASSE PAI ,NAO PODEM ADICIONAR NOVAS CHECKED EXCEPTION NA ASSINATURA DO METODO*/ 
class CannotJumpException extends Exception{}
class Jumper{
	public void jump()throws Exception{

	}
}
public class SuperJumper extends Jumper{
	public void jump() throws CannotJumpException{
		//pode sobrescrever o método com uma exceção menos restritiva
	}
	public static void main(String[] args){

	}
}
