class CustomException1 extends Exception{}
class CustomException2 extends Exception{}



public class MultiplasExcecoesComMulticatch{
	public static void lancarExcecao1()throws CustomException1{
		throw new CustomException1();
	}
	public static void lancarExcecao2()throws CustomException1{
		throw new CustomException2();
	}


	
	public static void main(String[] args){
  	System.out.println("Antes de invocar o metodo problemaPotencial() ");
	try{
		lancarExcecao1();
		lancarExcecao2();
		
		
	}catch(CustomException1| CustomException2 e){
	System.out.println("Uma exceção foi capturada.");
	}	

	
	}
 	
}