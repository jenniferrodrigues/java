public class EntendendoFluxoDecontroleComExceptionV2{
	public static void problemaPotencial(){
	System.out.println("Inicio de metodo problemaPotencial");
	throw new RuntimeException("Lancei uma runtimeException");
}
	public static void main(String[] args){
  	System.out.println("Antes de invocar o metodo problemaPotencial() ");
	try{
		problemaPotencial();
		System.out.println("APos invocar o metodo problemaPotencial() ");
	}catch(RuntimeException e){
	System.out.println("Executando o metodo problemaPotencial() ");
	e.printStackTrace();
	}
 	System.out.println("Apos o bloco try catach ");}}