class CustomException1 extends Exception{}
class CustomException2 extends Exception{}
class CustomException3 extends Exception{}


public class MultiplasExcecoes{
	public static void lancarExcecao1()throws CustomException1{
		throw new CustomException1();
	}
	public static void lancarExcecao2()throws CustomException1{
		throw new CustomException2();
	}
	public static void lancarExcecao3()throws CustomException1{
		throw new CustomException1();
	}

	
	public static void main(String[] args){
  	System.out.println("Antes de invocar o metodo problemaPotencial() ");
	try{
		lancarExcecao1();
		lancarExcecao2();
		lancarExcecao3();
		
	}catch(CustomException3 e){
	System.out.println("CustomException3 foi capturada.");
	}catch(CustomException2 e){
		System.out.println("CustomException3 foi capturada.");
	}catch(CustomException1 e){
		System.out.println("CustomException3 foi capturada.");
	}catch(Exception e){
	System.out.println("Exception foi capturada.");}

	
	}
 	
}