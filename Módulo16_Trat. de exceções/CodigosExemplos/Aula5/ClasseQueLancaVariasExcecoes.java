import java.rmi.MarshalException;

public class ClasseQueLancaVariasExcecoes{
    public void doAnything()throws MarshalException{
        throw new MarshalException("Lancando uma excecao customizada");
    }
    public static void main(String[] args) throws Exception{
    ClasseQueLancaVariasExcecoes c= new ClasseQueLancaVariasExcecoes ();
    try{
    c.doAnything();
    }catch(MarshalException e){

    System.out.println("Uma exceção do tipo MinhaException foi lançada  e tratada pelo programador");
    //imprime a pilha de invocacao no console para o programador identificar de onde a exception foi originada
    e.printStackTrace();
    }catch(Exception e){
    System.out.println("Uma exceção do tipo MinhaException foi lançada  e tratada pelo programador");
    e.printStackTrace();}}}
