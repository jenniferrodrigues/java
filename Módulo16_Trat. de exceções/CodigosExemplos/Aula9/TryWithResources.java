/*IMPORTANTE
-Disponível a partir do java 8
-Por questões de legibilidade de código e
facilidade de manutenção, o uso deste
recurso deve ser evitado.
*/
import java.io.FileInputStream;
import java.io.FileOutputStream;


public class TryWithResources{
	
	public static void main(String[] args){
		try{
			//so aceita recursos que implementem a interface java.Lang.Autocloseable
				FileInputStream fis =new FileInputStream("Arquivo Java");
				FileInputStream fos =new FileInputStream("Arquivo copia Java");
				)}
				//corpo do bloco try
				fis.close();//desnecessário,apenas esta aqui  para voce sabe que tem acesso a essas variaveis
				
	}
			
		} catch (Exception e) {
			//Tratamento de possivel exceção
		}finally{
			System.out.println("Codigo desnecessário");
		}



	}
}
