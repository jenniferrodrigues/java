import javax.swing.JOptionPane;

public class Dialog {

    public static void main(String[] args) {
        // pede para o usuário inserir seu nome
        String name = JOptionPane.showInputDialog("What is your name?");
        // cria a mensagem
        String message = String.format("Welcome, %s, to Java Programming", name);

        // Exibe a mensagem para cumprimentar o usuario pelo nome
        JOptionPane.showMessageDialog(null, message);
    }
}
