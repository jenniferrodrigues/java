
import javax.swing.JOptionPane;

public class AddInteger {

    public static void main(String[] args) {
        // pede para o usuário inserir o numero
        String number1 = JOptionPane.showInputDialog("Enter first integer:?");
        int number1IntFormat = Integer.parseInt(number1);

        // pede para o usuário inserir o numero 2
        String number2 = JOptionPane.showInputDialog("Enter second integer:?");
        int number2IntFormat = Integer.parseInt(number2);

        int sum = number1IntFormat + number2IntFormat;

        // cria a mensagem
        String message = String.format("The sum is: %s", sum);

        // Exibe a mensagem para cumprimentar o usuario pelo nome
        JOptionPane.showMessageDialog(null, message);
    }
}
