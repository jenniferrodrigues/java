package Modulo8;
import java.util.Scanner;
public class ContaBancariaTeste{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Vou solicitar algumas informações para abrir uma conta bancaria para voce, ok?\n");

		System.out.println("Digite o nome do banco:");
		String nomeDoBanco = scanner.nextLine();

		System.out.println("Digite o nome do correntista :");
		String nomeDoCorrentista = scanner.nextLine();

		System.out.println("Digite o numero da agencia: (numero inteiro)");
		int numeroDaAgencia = scanner.nextInt();

		System.out.println("Digite o numero da conta corrente: (numero inteiro)");
		int numeroDaContaCorrente = scanner.nextInt();

		System.out.println("Digite o saldo inicial da conta: (numero decimal, exemplo: 200,70");
		double saldoInicial = scanner.nextDouble();

		//criando o objeto com construtor personalizado
		ContaBancaria contaBancaria = new ContaBancaria(nomeDoCorrentista, nomeDoBanco, numeroDaAgencia, numeroDaContaCorrente, saldoInicial);
		System.out.println("Conta bancaria aberta com sucesso!\n\n");
		
		System.out.println("Digite o valor do depósito:");
		Double deposito = scanner.nextDouble();
 		//criando sem o construtor personalizado
		contaBancaria.depositar(deposito);

	

		System.out.println("Digite o valor do saque: ");
		Double saque = scanner.nextDouble();
		contaBancaria.efetuarSaque(saque);
	}
}