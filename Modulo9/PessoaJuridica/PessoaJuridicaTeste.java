import java.util.Scanner;
public class PessoaJuridicaTeste {
    public static void main(String[] args){
        Scanner read = new Scanner(System.in);
        System.out.println("Vou solicitar algumas informações pessoais, ok?\n");

        System.out.println("Digite o número do ID:");
        Long id= read.nextLong();

        System.out.println("Informe seu nome:");
        String razaoSocial = read.next();

        System.out.println("Digite o sobrenome:");
        String nomeFantasia= read.next();

        System.out.println("Digite o CPF:");
        String cnpj = read.next();

        //criando o objeto com construtor personalizado
        PessoaJuridica pessoaJuridica =new PessoaJuridica(id,razaoSocial,nomeFantasia,cnpj);
        System.out.println("Cadastro realizado com sucesso!\n\n");

        System.out.println("Id: " + pessoaJuridica.getId());
		System.out.println("Nome: " + pessoaJuridica.getRazaoSocial());
		System.out.println("Sobrenome: " + pessoaJuridica.getNomeFantasia());
	    

        //Duvida: por que o objeto com construtor nao retorna os get na chamada?
        PessoaJuridica dados = pessoaJuridica.criarPessoaFisica(id, razaoSocial,nomeFantasia, cnpj);
        System.out.println(dados);
        System.out.println("Dados: "+pessoaJuridica.criarPessoaFisica(id, razaoSocial, nomeFantasia, cnpj));
       



    }

    
}
