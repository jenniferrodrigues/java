public class PessoaJuridica {
    public Long id;
    public String razaoSocial;
    public String nomeFantasia;
    public String cnpj;

    public PessoaJuridica() {

    }

    public PessoaJuridica(Long novoId, String novoRazaoSocial, String novoNomeFantasia, String novoCnpj) {
        id = novoId;
        razaoSocial = novoRazaoSocial;
        nomeFantasia = novoNomeFantasia;
        cnpj = novoCnpj;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long _id) {
        id = _id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String _razaoSocial ){
        razaoSocial = _razaoSocial;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String _nomeFantasia ){
        nomeFantasia = _nomeFantasia;
    }

    public void setCnpj(String _cnpj) {
        cnpj = _cnpj;

    }

    public String getCnpj() {
        return cnpj;
    }

    public PessoaJuridica criarPessoaFisica(Long novoId, String novoRazaoSocial, String novoNomeFantasia, String novoCnpj) {
        PessoaJuridica pessoa = new PessoaJuridica();
        pessoa.getId();
        pessoa.getRazaoSocial();
        pessoa.getNomeFantasia();
        pessoa.getCnpj();
        return pessoa;

    }

}