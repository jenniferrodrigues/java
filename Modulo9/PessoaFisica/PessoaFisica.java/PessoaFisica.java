public class PessoaFisica {
    public Long id;
    public String nome;
    public String sobrenome;
    public String cpf;

    public PessoaFisica() {

    }

    public PessoaFisica(Long novoId, String novoNome, String novoSobrenome, String novoCpf) {
        id = novoId;
        nome = novoNome;
        sobrenome = novoSobrenome;
        cpf = novoCpf;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long _id) {
        id = _id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String _nome ){
        nome = _nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String _sobrenome ){
        sobrenome = _sobrenome;
    }

    public void setCpf(String _cpf) {
        cpf = _cpf;

    }

    public String getCpf() {
        return cpf;
    }

    public PessoaFisica criarPessoaFisica(Long novoId, String novoNome, String novoSobrenome, String novoCpf) {
        PessoaFisica pessoa = new PessoaFisica();
        pessoa.getId();
        pessoa.getNome();
        pessoa.getSobrenome();
        pessoa.getCpf();
        return pessoa;

    }

}