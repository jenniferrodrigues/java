import java.util.Scanner;
public class PessoaFisicaTeste {
    public static void main(String[] args){
        Scanner read = new Scanner(System.in);
        System.out.println("Vou solicitar algumas informações pessoais, ok?\n");

        System.out.println("Digite o número do ID:");
        Long id= read.nextLong();

        System.out.println("Informe seu nome:");
        String nome = read.next();

        System.out.println("Digite o sobrenome:");
        String sobrenome= read.next();

        System.out.println("Digite o CPF:");
        String cpf = read.next();

        //criando o objeto com construtor personalizado
        PessoaFisica pessoaFisica =new PessoaFisica(id,nome,sobrenome,cpf);
        System.out.println("Cadastro realizado com sucesso!\n\n");
    

        //Duvida: por que o objeto com construtor nao retorna os get na chamada?
        /*tentativa 1*/
        PessoaFisica dados = pessoaFisica.criarPessoaFisica(id, nome, sobrenome, cpf);
        System.out.println(dados);
        System.out.println("Dados: "+dados.criarPessoaFisica(id, nome, sobrenome, cpf));

        /*tentativa 2*/
        System.out.println("Dados: "+pessoaFisica.criarPessoaFisica(id, nome, sobrenome, cpf));
       
        /*modo correto-unico modo que funciona*/
        System.out.println("Id: " + pessoaFisica.getId());
		System.out.println("Nome: " + pessoaFisica.getNome());
		System.out.println("Sobrenome: " + pessoaFisica.getSobrenome());



    }

    
}
