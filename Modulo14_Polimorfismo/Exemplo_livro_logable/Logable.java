public interface Logable{
    public String getInitInfo();
    public String getLogableEvent();
}