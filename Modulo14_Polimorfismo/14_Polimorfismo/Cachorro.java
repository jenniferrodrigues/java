public class Cachorro extends Animal{
    public Cachorro(){}
    public Cachorro(int id, String comida){
        super(id,comida);
    }

    public void brincar(){
        System.out.println("Cachorro correndoa atrás do próprio rabo...");
    }
    public void emitirSom(){
        System.out.println("Cachorro emitindo som.");
    }

    
}

