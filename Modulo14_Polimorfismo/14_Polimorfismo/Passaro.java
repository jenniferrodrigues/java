public class Passaro extends Animal {
    public Passaro(){}
    public Passaro(int id, String comidaPreferida){
        super(id, comidaPreferida);
    }
    public void voar(){
        System.out.println("Pássaro voando.");
    }
}
