-MAPA MENTAL DO CAP POLIMORFISMO:https://www.mindmeister.com/1728667921?t=7s7OTKE0h5
-Polimorfismo:Várias formas-Objeto pode assumir diferentes papeis.
Um objeto tem o comportamento da classe pai em uma 
determinada situação.
Se o metodo é um IS A for válido, pode ser aplicado. 
Por exemplo: Animal-classe pai, cachorro,passaro-filhas
cachorro é um animal=ok
passaro e um animal=ok

 Animal cachorro = new Cachorro(1, "Picanha");//válido porque:IS A=é um
 cachorro.comer(":"+"carne");//válido porque o método comer está na classe Animal
 cachorro.dormir();//valido pq dormir esta na classe animal
 cachorro.emitirSom();//valido pq emitirsom esta na classe animal

-Implements:
significa que implementa uma interface. O projeto deve
seguir o mesmo padrao de desenvolvimento.

-Extends:significa herança da classe pai. 

-CASTING:Converte um objeto cachorro( que é do tipo animal)
em um tipo cachorro. Tem acesso aos metodos da classe cachorro.
 ((Cachorro).cachorro).brincar();


- Como sobrescrever métodos (override)
Regras para sobrescrição de métodos (override)
*Mesmo nome de método
*Mesmo tipo de retorno
*Mesma lista de argumentos
*Não pode ser menos acessível (mais
restritivo) que o método que ele
sobrescreve


- Método covariante:
O tipo de retorno do método sobrescrito na subclasse deve ser um subtipo do tipo
de retorno do método da superclasse.

Pode mudar o tipo de retorno da classe filha. Desde que o tipo de retorno seja um 
tipo de retorno da superclasse.
public class StuffA{
  public StuffA doStuff(){
	return new StuffA();}}

public class StuffB sxtends StuffA{
	public StuffB doStuff(){//tipo de retorno StuffB permitido, pois faz parte da classe filha.
		return new StuffB();}}//sobescrita valida a partir do java8


//NAO RODA
public class Stuffc sxtends StuffA{
	public oBJECT doStuff(){//tipo de retorno oBJECT NAO E UM TIPO DE RETORNO DA CLASSE STUFF A , pois NAO faz parte da classe filha.
		return new StuffC();}}

-ARRAY HETEROGÊNEO
Array homogêneo:São arrays que contém objetos do mesmo tipo de classe.
Ex.:MyDate[] dates = new MyDate[2];
dates[0] = new MyDate(22,12,1985);
dates[1] =  new MyDate(22,6,1985);

Array hetrogêneo:É um array com objetos com tipos de classe diferentes.
Ex.:Animal[] animais = new Animal[1024];
animais[0] = new Animal();
animais[1] = new Pato();
animais[2] = new Cachorro();
animais[3] = new Gato();


-Conversão de tipos usando polimorfismo
Operador instanceof-usado antes de fazer o casting.
Garante que o objeto que está sendo armazenado naquela variável 
é de um tipo específico.
IMPORTANTE:
-Sempre utilize o operador instanceof antes
de tentar converter um objeto para testar
se o objeto referenciado é de um tipo
específico.
-Você consegue ter acesso a todas as
funcionalidades do objeto convertendo-o
para o tipo específico
-Passos para verificar se o casting é seguro
e apropriado:

Passos para verificar se o casting é seguro
e apropriado:
-Conversões para cima da hierarquia são
sempre efetuadas implicitamente. O
Casting é desnecessário.

-Para realizar conversões para baixo da
hierarquia de herança, a conversão deve
ser para um tipo que seja uma subclasse
verificada pelo compilador.
-Em conversões, o tipo de objeto é sempre
checado somente em tempo de execução

public class TesteCasting {
    public static void main(String args[]){
        //tem acesso somente aos metods da classe animal, pois chamou o tipo de referencia Animal
        //variavel animal é instanciada com objeto  tipo Cachorro
        Animal animal = new Cachorro();//válido porque:IS A=é um

        //variavel animal2 sendo instanciada com o objeto tipo passaro
        Animal animal2=new Passaro();

        //Nova variavel cachorro sendo declarada do tipo Cachorro.
        // Converte animal para o tipo cachorro
        Cachorro cachorro = (Cachorro)animal;//ok animal é um cachorro
        cachorro.brincar();//válido porque o método comer está na classe Animal
        
        //variavel do tipo passaro sendo instanciado pelo objeto refenciado pela variavel animal2 
        //que e um passaro
        Passaro passaro =(Passaro)animal2;//ok animal2 é um passaro
        passaro.voar();

        //variavel passaro sendo inicializada com passaro 2 e inicializando com 1
        Passaro passaro2 = null;
        
        //Animal nao e um passaro. O if nao executara
        //instanceof verifica o tipo de variavel
        //se o objeto animal for uma instancia do tipo passaro
        if(animal instanceof Passaro){
            passaro2 = (Passaro)animal;//pode converter de animal para passaro
            passaro2.voar();
        }else{
            System.out.println(" Animal não é um pássaro.");
        }
      
    


    }

    
}








	


       