public class TesteArrayHeterogeneo {
    public static void main(String[] args) {
        Animal[] animais = new Animal[4];
        animais[0] = new Cachorro();//valido,pois gato é-Um animal
        animais[1]= new Gato();
        animais[2]= new Pato();
        animais[3]= new Animal();

        for(Animal animal: animais){
            animal.tipoAnimal();
        }
    }

    }
    

