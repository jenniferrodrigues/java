public class Animal{
    public String comidaPreferida;
    public int id;

    public Animal(){

    }
    public Animal(int id, String comidaPreferida){
        this.id=id;
        this.comidaPreferida=comidaPreferida;

    }
    public void emitirSom(){
        System.out.println("Animal emitindo som.");
    }

    public void comer(  String comidaPreferida){
        System.out.println("Animal comendo"+comidaPreferida );
    }

    void dormir(){
        System.out.println("Animal dormindo.");
    }

    
}