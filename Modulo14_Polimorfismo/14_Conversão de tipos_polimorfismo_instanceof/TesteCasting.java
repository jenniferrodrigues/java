public class TesteCasting {
    public static void main(String args[]){
        //tem acesso somente aos metods da classe animal, pois chamou o tipo de referencia Animal
        //variavel animal é instanciada com objeto  tipo Cachorro
        Animal animal = new Cachorro();//válido porque:IS A=é um

        //variavel animal2 sendo instanciada com o objeto tipo passaro
        Animal animal2=new Passaro();

        //Nova variavel cachorro sendo declarada do tipo Cachorro.
        // Converte animal para o tipo cachorro
        Cachorro cachorro = (Cachorro)animal;//ok animal é um cachorro
        cachorro.brincar();//válido porque o método comer está na classe Animal
        
        //variavel do tipo passaro sendo instanciado pelo objeto refenciado pela variavel animal2 
        //que e um passaro
        Passaro passaro =(Passaro)animal2;//ok animal2 é um passaro
        passaro.voar();

        //variavel passaro sendo inicializada com passaro 2 e inicializando com 1
        Passaro passaro2 = null;
        
        //Animal nao e um passaro. O if nao executara
        //instanceof verifica o tipo de variavel
        //se o objeto animal for uma instancia do tipo passaro
        if(animal instanceof Passaro){
            passaro2 = (Passaro)animal;//pode converter de animal para passaro
            passaro2.voar();
        }else{
            System.out.println(" Animal não é um pássaro.");
        }
      
    


    }

    
}
