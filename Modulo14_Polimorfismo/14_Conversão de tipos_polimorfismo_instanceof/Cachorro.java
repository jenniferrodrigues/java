public class Cachorro extends Animal{
    public Cachorro(){}
    public Cachorro(int id, String comidaPreferida){
        super(id,comidaPreferida);
    }

    public void brincar(){
        System.out.println("Cachorro correndo atrás do próprio rabo...");
    }
    public void emitirSom(){
        System.out.println("Cachorro emitindo som.");
    }

    
}

