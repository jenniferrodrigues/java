public class Animal{
    public String comida;
    public int id;

    public Animal(){

    }
    public Animal(int id, String comida){
        this.id=id;
        this.comida=comida;

    }
    public void emitirSom(){
        System.out.println("Animal emitindo som.");
    }

    public void comer(  String comida){
        System.out.println("Animal comendo"+comida );
    }

    void dormir(){
        System.out.println("Animal dormindo.");
    }

    
}