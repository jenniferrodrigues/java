public class TestePolimorfismoCasting {
 
    public static void main(String args[]){
        //tem acesso somente aos metods da classe animal, pois chamou o tipo de referencia Animal
        Animal cachorro = new Cachorro(1, "Picanha");//válido porque:IS A=é um
        cachorro.comer(":"+"carne");//válido porque o método comer está na classe Animal
        cachorro.dormir();//valido pq dormir esta na classe animal
        cachorro.emitirSom();//valido pq emitirsom esta na classe animal
        
        //CASTING:Converte um objeto cachorro( que é do tipo animal)      
        //em um tipo cachorro. Tem acesso aos metodos da classe cachorro
        ((Cachorro)cachorro).brincar();

        Animal passaro = new Passaro(2,"Pão");
        passaro.comer (":"+"Alpiste");
        passaro.dormir();
        passaro.emitirSom();
       // 
       ((Passaro)passaro).voar();
    


    }

    
}
